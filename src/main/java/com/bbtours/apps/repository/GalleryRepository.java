package com.bbtours.apps.repository;

import org.springframework.data.repository.CrudRepository;

import com.bbtours.apps.model.Gallery;

public interface GalleryRepository extends CrudRepository<Gallery,Long>{
	

}
