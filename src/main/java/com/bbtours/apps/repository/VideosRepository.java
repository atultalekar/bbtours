package com.bbtours.apps.repository;

import org.springframework.data.repository.CrudRepository;

import com.bbtours.apps.model.Videos;

public interface VideosRepository extends CrudRepository<Videos,Long>{

}
