package com.bbtours.apps.repository;

import org.springframework.data.repository.CrudRepository;

import com.bbtours.apps.model.Offers;

public interface OffersRepository extends CrudRepository<Offers,Long>{

}
