package com.bbtours.apps.repository;

import org.springframework.data.repository.CrudRepository;

import com.bbtours.apps.model.Enquiry;

public interface EnquiryRepository extends CrudRepository<Enquiry,Long>{

}
