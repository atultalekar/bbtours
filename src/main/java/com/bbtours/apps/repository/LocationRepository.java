package com.bbtours.apps.repository;

import org.springframework.data.repository.CrudRepository;

import com.bbtours.apps.model.Location;

public interface LocationRepository extends CrudRepository<Location,Long>{

}
