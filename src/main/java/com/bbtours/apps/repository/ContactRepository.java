package com.bbtours.apps.repository;

import org.springframework.data.repository.CrudRepository;

import com.bbtours.apps.model.Contact;

public interface ContactRepository extends CrudRepository<Contact,Long> {

}
