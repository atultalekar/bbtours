package com.bbtours.apps.repository;

import org.springframework.data.repository.CrudRepository;

import com.bbtours.apps.model.Events;

public interface EventsRepository extends CrudRepository<Events,Long> {

}
