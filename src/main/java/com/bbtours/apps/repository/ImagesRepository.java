package com.bbtours.apps.repository;

import org.springframework.data.repository.CrudRepository;

import com.bbtours.apps.model.Images;

public interface ImagesRepository  extends CrudRepository<Images,Long>{

}
