package com.bbtours.apps;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class BbtoursApplication {

	public static void main(String[] args) {
		SpringApplication.run(BbtoursApplication.class, args);
	}

}

