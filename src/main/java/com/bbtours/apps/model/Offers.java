package com.bbtours.apps.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="offers")
public class Offers {
	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	@Column(name="off_id")
	private String offId;
	@Column(name="off_name")
	private String offName;
	@Column(name="off_desc")
	private String offDesc;
	@Column(name="off_date")
	private String offDate;
	
	public Offers() {
		super();
		// TODO Auto-generated constructor stub
	}
	public String getOffId() {
		return offId;
	}
	public void setOffId(String offId) {
		this.offId = offId;
	}
	public String getOffName() {
		return offName;
	}
	public void setOffName(String offName) {
		this.offName = offName;
	}
	public String getOffDesc() {
		return offDesc;
	}
	public void setOffDesc(String offDesc) {
		this.offDesc = offDesc;
	}
	public String getOffDate() {
		return offDate;
	}
	public void setOffDate(String offDate) {
		this.offDate = offDate;
	}
	public Offers(String offId, String offName, String offDesc, String offDate) {
		super();
		this.offId = offId;
		this.offName = offName;
		this.offDesc = offDesc;
		this.offDate = offDate;
	}

}
