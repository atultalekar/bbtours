package com.bbtours.apps.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
public class Location {
	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	@Column(name="loc_id")
	private String locId;
	@Column(name="loc_place")
	private String locPlace;
	
	public Location() {
		super();
		// TODO Auto-generated constructor stub
	}
	public String getLocId() {
		return locId;
	}
	public void setLocId(String locId) {
		this.locId = locId;
	}
	public String getLocPlace() {
		return locPlace;
	}
	public void setLocPlace(String locPlace) {
		this.locPlace = locPlace;
	}
	public Location(String locId, String locPlace) {
		super();
		this.locId = locId;
		this.locPlace = locPlace;
	}

}
