package com.bbtours.apps.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="contact")
public class Contact {
	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	@Column(name="cont_id")
	private String contId;
	@Column(name="cont_name")
	private String contName;
	@Column(name="cont_phone")
	private String contPhone;
	@Column(name="cont_msg")
	private String contMsg;
	
	
	public Contact() {
		super();
		// TODO Auto-generated constructor stub
	}
	public String getContId() {
		return contId;
	}
	public void setContId(String contId) {
		this.contId = contId;
	}
	public String getContName() {
		return contName;
	}
	public void setContName(String contName) {
		this.contName = contName;
	}
	public String getContPhone() {
		return contPhone;
	}
	public void setContPhone(String contPhone) {
		this.contPhone = contPhone;
	}
	public String getContMsg() {
		return contMsg;
	}
	public void setContMsg(String contMsg) {
		this.contMsg = contMsg;
	}
	public Contact(String contId, String contName, String contPhone, String contMsg) {
		super();
		this.contId = contId;
		this.contName = contName;
		this.contPhone = contPhone;
		this.contMsg = contMsg;
	}
}
