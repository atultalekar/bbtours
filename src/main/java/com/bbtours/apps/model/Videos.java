package com.bbtours.apps.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;

@Entity
public class Videos {
	@Id
	@Column(name="vid_id")
	private String vidId;
	@Column(name="vid_name")
	private String vidName;
	@Column(name="vid_desc")
	private String vidDesc;
	
	public Videos() {
		super();
		// TODO Auto-generated constructor stub
	}
	public String getVidId() {
		return vidId;
	}
	public void setVidId(String vidId) {
		this.vidId = vidId;
	}
	public String getVidName() {
		return vidName;
	}
	public void setVidName(String vidName) {
		this.vidName = vidName;
	}
	public String getVidDesc() {
		return vidDesc;
	}
	public void setVidDesc(String vidDesc) {
		this.vidDesc = vidDesc;
	}
	public Videos(String vidId, String vidName, String vidDesc) {
		super();
		this.vidId = vidId;
		this.vidName = vidName;
		this.vidDesc = vidDesc;
	}
	
}
