package com.bbtours.apps.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
@Entity
@Table(name="images")
public class Images {
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	@Column(name="img_id")
	private String imgId;
	
	@Column(name="img_name")
	private String imgName;
	@Column(name="img_desc")
	private String imgDesc;
	
	public Images() {
		super();
		// TODO Auto-generated constructor stub
	}

	public String getImgId() {
		return imgId;
	}
	
	public void setImgId(String imgId) {
		this.imgId = imgId;
	}
	public String getImgName() {
		return imgName;
	}
	public void setImgName(String imgName) {
		this.imgName = imgName;
	}
	public String getImgDesc() {
		return imgDesc;
	}
	public void setImgDesc(String imgDesc) {
		this.imgDesc = imgDesc;
	}
	public Images(String imgId, String imgName, String imgDesc) {
		super();
		this.imgId = imgId;
		this.imgName = imgName;
		this.imgDesc = imgDesc;
	}
	
}