package com.bbtours.apps.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="gallery")
public class Gallery {
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private Long galId;	
	
	@Column(name="gal_name")
	private String galName;
	
	
	public Gallery() {
		super();
		// TODO Auto-generated constructor stub
	}
	public Long getGalId() {
		return galId;
	}
	public void setGalId(Long galId) {
		this.galId = galId;
	}
	public String getGalName() {
		return galName;
	}
	public void setGalName(String galName) {
		this.galName = galName;
	}
	public Gallery(Long galId, String galName) {
		super();
		this.galId = galId;
		this.galName = galName;
	}
}
