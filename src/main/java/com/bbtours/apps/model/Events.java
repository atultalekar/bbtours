package com.bbtours.apps.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="events")
public class Events {
	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	@Column(name="eve_id")
	private String eveId;
	@Column(name="eve_name")
	private String eveName;
	@Column(name="eve_desc")
	private String eveDesc;
	
	public Events() {
		super();
		// TODO Auto-generated constructor stub
	}
	public String getEveId() {
		return eveId;
	}
	public void setEveId(String eveId) {
		this.eveId = eveId;
	}
	public String getEveName() {
		return eveName;
	}
	public void setEveName(String eveName) {
		this.eveName = eveName;
	}
	public String getEveDesc() {
		return eveDesc;
	}
	public void setEveDesc(String eveDesc) {
		this.eveDesc = eveDesc;
	}
	public Events(String eveId, String eveName, String eveDesc) {
		super();
		this.eveId = eveId;
		this.eveName = eveName;
		this.eveDesc = eveDesc;
	}

}
