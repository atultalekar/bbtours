package com.bbtours.apps.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="enquiry")
public class Enquiry {
	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	@Column(name="enq_id")
	private String enqId;
	@Column(name="enq_name")
	private String enqName;
	@Column(name="enq_phone")
	private String enqPhone;
	@Column(name="enq_info")
	private String enqInfo;
	
	public Enquiry() {
		super();
		// TODO Auto-generated constructor stub
	}
	public String getEnqId() {
		return enqId;
	}
	public void setEnqId(String enqId) {
		this.enqId = enqId;
	}
	public String getEnqName() {
		return enqName;
	}
	public void setEnqName(String enqName) {
		this.enqName = enqName;
	}
	public String getEnqPhone() {
		return enqPhone;
	}
	public void setEnqPhone(String enqPhone) {
		this.enqPhone = enqPhone;
	}
	public String getEnqInfo() {
		return enqInfo;
	}
	public void setEnqInfo(String enqInfo) {
		this.enqInfo = enqInfo;
	}
	public Enquiry(String enqId, String enqName, String enqPhone, String enqInfo) {
		super();
		this.enqId = enqId;
		this.enqName = enqName;
		this.enqPhone = enqPhone;
		this.enqInfo = enqInfo;
	}
}
