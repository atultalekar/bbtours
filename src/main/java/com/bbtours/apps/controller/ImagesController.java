package com.bbtours.apps.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import com.bbtours.apps.model.Images;
import com.bbtours.apps.service.ImagesService;



@Controller
//@RequestMapping(value="/images", method=RequestMethod.GET)
public class ImagesController {
	//@Value("${text.message}")
		private String message;
		
	 @GetMapping("/images")
	    public String main(Model model) {
	        model.addAttribute("message", message);
	        //model.addAttribute(firstName, firstName);
	        //System.out.println(firstName);
	        return "images"; //view
	 }
	
	@Autowired
	ImagesService imagesService;
	
	@RequestMapping(value="/addImages", method=RequestMethod.GET)
	public ModelAndView addImages () {
		ModelAndView model=new ModelAndView ();
		Images images=new Images ();
		model.addObject("imagesForm",images);
		model.setViewName("images_form");
		System.out.println(model);
		return model;
	}

}