package com.bbtours.apps.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import com.bbtours.apps.model.Gallery;
import com.bbtours.apps.service.GalleryService;

@Controller
//@RequestMapping(value="/gallery", method=RequestMethod.GET)
public class GalleryController {
	
	//@Value("${text.message}")
	private String message;
	
	 @GetMapping("/gallery")
	    public String main(Model model) {
	        model.addAttribute("message", message);
	        //model.addAttribute(firstName, firstName);
	        //System.out.println(firstName);
	        return "gallery"; //view
	 }
	@Autowired
	GalleryService galleryService;
    
	@RequestMapping(value="/addGallery", method=RequestMethod.GET)
	public ModelAndView addGallery () {
		ModelAndView model=new ModelAndView ();
		Gallery gallery=new Gallery ();
		model.addObject("galleryForm",gallery);
		model.setViewName("gallery_form");
		System.out.println(model);
		return model;
	}

}
