package com.bbtours.apps.controller;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class WelcomeController {
	@Value("${welcome.message}")
	private String message;
	
	 @GetMapping("/")
	    public String main(Model model) {
	        model.addAttribute("message", message);
	        //model.addAttribute(firstName, firstName);
	        //System.out.println(firstName);
	        return "welcome"; //view
	 }

}
