package com.bbtours.apps.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import com.bbtours.apps.model.Enquiry;
import com.bbtours.apps.service.EnquiryService;

@Controller
//@RequestMapping(value="/enquiry", method=RequestMethod.GET)
public class EnquiryController {
	//@Value("${text.message}")
		private String message;
		
		 @GetMapping("/enquiry")
		    public String main(Model model) {
		        model.addAttribute("message", message);
		        //model.addAttribute(firstName, firstName);
		        //System.out.println(firstName);
		        return "enquiry"; //view
		 }
	@Autowired
	EnquiryService enquiryService;
	
	@RequestMapping(value="/addEnquiry", method=RequestMethod.GET)
	public ModelAndView addEnquiry () {
		ModelAndView model=new ModelAndView ();
		Enquiry enquiry=new Enquiry ();
		model.addObject("enquiryForm",enquiry);
		model.setViewName("enquiry_form");
		System.out.println(model);
		return model;
	}

}