package com.bbtours.apps.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import com.bbtours.apps.model.Offers;
import com.bbtours.apps.service.OffersService;

@Controller
//@RequestMapping(value="/offers", method=RequestMethod.GET)
public class OffersController {
	//@Value("${text.message}")
	private String message;
	
	 @GetMapping("/offers")
	    public String main(Model model) {
	        model.addAttribute("message", message);
	        //model.addAttribute(firstName, firstName);
	        //System.out.println(firstName);
	        return "offers"; //view
	 }
	@Autowired
	OffersService offersService;
	
	@RequestMapping(value="/addOffers", method=RequestMethod.GET)
	public ModelAndView addOffers () {
		ModelAndView model=new ModelAndView ();
		Offers offers=new Offers ();
		model.addObject("offersForm",offers);
		model.setViewName("offers_form");
		System.out.println(model);
		return model;
	}

}