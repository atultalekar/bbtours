package com.bbtours.apps.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import com.bbtours.apps.model.Location;
import com.bbtours.apps.service.LocationService;



@Controller
@RequestMapping(value="/location", method=RequestMethod.GET)
public class LocationController {
	
	@Autowired
	LocationService locationService;
	
	@RequestMapping(value="/addLocation", method=RequestMethod.GET)
	public ModelAndView addLocation () {
		ModelAndView model=new ModelAndView ();
		Location location=new Location ();
		model.addObject("locationForm",location);
		model.setViewName("location_form");
		System.out.println(model);
		return model;
	}

}
