package com.bbtours.apps.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import com.bbtours.apps.model.Events;
import com.bbtours.apps.service.EventsService;

@Controller
@RequestMapping(value="/events", method=RequestMethod.GET)
public class EventsController {
	
	@Autowired
	EventsService eventsService;
	
	@RequestMapping(value="/addEvents", method=RequestMethod.GET)
	public ModelAndView addEvents () {
		ModelAndView model=new ModelAndView ();
		Events events=new Events ();
		model.addObject("eventsForm",events);
		model.setViewName("events_form");
		System.out.println(model);
		return model;
	}

}