package com.bbtours.apps.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import com.bbtours.apps.model.Videos;
import com.bbtours.apps.service.VideosService;

@Controller
//@RequestMapping(value="/videos", method=RequestMethod.GET)
public class VideosController {
	
	//@Value("${text.message}")
		private String message;
		
		 @GetMapping("/videos")
		    public String main(Model model) {
		        model.addAttribute("message", message);
		        //model.addAttribute(firstName, firstName);
		        //System.out.println(firstName);
		        return "videos"; //view
		 }
	
	@Autowired
	VideosService videosService;
	
	@RequestMapping(value="/addVideos", method=RequestMethod.GET)
	public ModelAndView addVideos () {
		ModelAndView model=new ModelAndView ();
		Videos videos=new Videos ();
		model.addObject("videosForm",videos);
		model.setViewName("videos_form");
		System.out.println(model);
		return model;
	}

}