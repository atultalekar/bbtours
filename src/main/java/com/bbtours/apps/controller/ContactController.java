package com.bbtours.apps.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import com.bbtours.apps.model.Contact;
import com.bbtours.apps.service.ContactService;

@Controller
//@RequestMapping(value="/contact", method=RequestMethod.GET)
public class ContactController {
	@Autowired
	ContactService contactService;
	
		
	@RequestMapping(value="/addContact", method=RequestMethod.GET)
	public ModelAndView addContact () {
		ModelAndView model=new ModelAndView ();
		Contact contact=new Contact ();
		model.addObject("contactForm",contact);
		model.setViewName("contact_form");
		System.out.println(model);
		return model;
	}
	//@Value("${text.message}")
			private String message;
			
			 @GetMapping("/contact")
			    public String main(Model model) {
			        model.addAttribute("message", message);
			        //model.addAttribute(firstName, firstName);
			        //System.out.println(firstName);
			        return "contact"; //view
			 }
		


}