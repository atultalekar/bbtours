package com.bbtours.apps.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.bbtours.apps.model.Videos;
import com.bbtours.apps.repository.VideosRepository;

@Service
@Transactional
public class VideosServiceImpl implements VideosService {
	
	@Autowired
	VideosRepository videosRepository;

	@Override
	public void add(Videos videos) {
		
		videosRepository.save(videos);
	}
	

	
	
}	 
