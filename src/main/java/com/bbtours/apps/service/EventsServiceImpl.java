package com.bbtours.apps.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.bbtours.apps.model.Events;
import com.bbtours.apps.repository.EventsRepository;

@Service
@Transactional
public class EventsServiceImpl implements EventsService {
	
	@Autowired
	EventsRepository eventsRepository;

	@Override
	public void add(Events events) {
		
		eventsRepository.save(events);
	}
	

	
	
}	 
