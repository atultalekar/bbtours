package com.bbtours.apps.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.bbtours.apps.model.Location;
import com.bbtours.apps.repository.LocationRepository;


@Service
@Transactional
public class LocationServiceImpl implements LocationService {
	
	@Autowired
	LocationRepository locationRepository;

	@Override
	public void add(Location location) {
		
		locationRepository.save(location);
	}
	

	
	
}	 

