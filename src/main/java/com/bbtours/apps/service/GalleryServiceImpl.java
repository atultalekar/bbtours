package com.bbtours.apps.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.bbtours.apps.model.Gallery;
import com.bbtours.apps.repository.GalleryRepository;

@Service
@Transactional
public class GalleryServiceImpl implements GalleryService {
	
	@Autowired
	GalleryRepository galleryRepository;

	@Override
	public void add(Gallery gallery) {
		
		galleryRepository.save(gallery);
	}
	

	
	
}	 
