package com.bbtours.apps.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.bbtours.apps.model.Enquiry;
import com.bbtours.apps.repository.EnquiryRepository;

@Service
@Transactional
public class EnquiryServiceImpl implements EnquiryService {
	
	@Autowired
	EnquiryRepository enquiryRepository;

	@Override
	public void add(Enquiry enquiry) {
		
		enquiryRepository.save(enquiry);
	}
	

	
	
}	 
