package com.bbtours.apps.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.bbtours.apps.model.Offers;
import com.bbtours.apps.repository.OffersRepository;

@Service
@Transactional
public class OffersServiceImpl implements OffersService {
	
	@Autowired
	OffersRepository offersRepository;

	@Override
	public void add(Offers offers) {
		
		offersRepository.save(offers);
	}
	

	
	
}	 

