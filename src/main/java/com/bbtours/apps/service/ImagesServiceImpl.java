package com.bbtours.apps.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.bbtours.apps.model.Images;
import com.bbtours.apps.repository.ImagesRepository;

@Service
@Transactional
public class ImagesServiceImpl implements ImagesService {
	
	@Autowired
	ImagesRepository imagesRepository;

	@Override
	public void add(Images images) {
		
		imagesRepository.save(images);
	}
	

	
	
}	 
