<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
    <%@ taglib uri ="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri ="http://www.springframework.org/tags" prefix="spring"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Add offers</title>
	<link href="../../webjars/bootstrap/3.3.7/css/bootstrap.min.css">
	
</body>
</head>
<body>
	<div class="container">
	<spring:url value="/offers/saveOffers" var="saveURL" />
	<h2>Offers</h2>
	<form:form modleAttibute="offersForm" method="post" action="${saveURL }" cssClass="form" >
		<form:hidden path="id"/>
		<div class="form-group">
			<label>Title</label>
			<form:input path="title" cssClass="form-control" id="title" />
		</div>
		<div class="form-group">
			<label>Category</label>
			<form:input path="category" cssClass="from-control" id="category" />
		</div>
		<button type="submit" class="btn btn-primary">save</button>
	</form:form>
	
	</div>
	<script  src="../../webjars/bootstrap/3.3.7/js/bootstrap.min.js"></script>
	<script  src="../../webjars/jquery/1.11.1/jquery.min.js"></script>	
</body>
</html>